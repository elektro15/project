library ieee;
use ieee.std_logic_1164.all;

entity decoder_basys is
  port (
    ---- inputs ----
    -- input data bits
    con1, con2, con3, con4, con5, con6, con7, con8,
    con9, con10, con11, con12, con13 : in std_logic;
    -- input switches/buttons
    sw1, sw2, sw3, sw4, sw5, sw6, sw7, sw8,
    btn1, btn2, btn3, btn4 : in std_logic;
    ---- output ----
    -- output leds
    led1, led2, led3, led4, led5, led6, led7, led8 : out std_logic;
    -- output digit displays (segment/digits)
    segdp, seg1, seg2, seg3, seg4, seg5, seg6, seg7 : out std_logic;
    dgt1, dgt2, dgt3, dgt4 : out std_logic);
end decoder_basys;

architecture arch of decoder_basys is
  component decoder
    port (i1, i2, i3, i4, i5, i6, i7, i8 : in  std_logic;
          i9, i10, i11, i12, i13         : in  std_logic;
          o1, o2, o3, o4, o5, o6, o7, o8 : out std_logic;
          errordouble                    : out std_logic);
  end component;

  signal i1, i2, i3, i4, i5, i6, i7, i8, i9, i10, i11, i12, i13 : std_logic;
  signal o1, o2, o3, o4, o5, o6, o7, o8, errordouble : std_logic;
begin
  -- Pipe inputs from connectors, introduce errors with switches/buttons
  i1  <= con1  xor sw1 ;
  i2  <= con2  xor sw2 ;
  i3  <= con3  xor sw3 ;
  i4  <= con4  xor sw4 ;
  i5  <= con5  xor sw5 ;
  i6  <= con6  xor sw6 ;
  i7  <= con7  xor sw7 ;
  i8  <= con8  xor sw8 ;
  i9  <= con9  xor btn1;
  i10 <= con10 xor btn2;
  i11 <= con11 xor btn3;
  i12 <= con12 xor btn4;
  i13 <= con13; -- not switch/button available for error in overall parity

  -- hook up encoder component instance
  instance: decoder PORT MAP (
    i1, i2, i3, i4, i5, i6, i7, i8, i9, i10, i11, i12, i13,
    o1, o2, o3, o4, o5, o6, o7, o8, errordouble
  );

  -- LEDs display output or nothing on double error
  led1 <= o1 and not errordouble;
  led2 <= o2 and not errordouble;
  led3 <= o3 and not errordouble;
  led4 <= o4 and not errordouble;
  led5 <= o5 and not errordouble;
  led6 <= o6 and not errordouble;
  led7 <= o7 and not errordouble;
  led8 <= o8 and not errordouble;

  -- display full digits on double error instead of LED outputs
  dgt1 <= not errordouble;
  dgt2 <= not errordouble;
  dgt3 <= not errordouble;
  dgt4 <= not errordouble;

  -- do somethng cool when double error???
  segdp <= '0'; -- low means show
  seg1  <= '0'; -- low means show
  seg2  <= '0'; -- low means show
  seg3  <= '0'; -- low means show
  seg4  <= '0'; -- low means show
  seg5  <= '0'; -- low means show
  seg6  <= '0'; -- low means show
  seg7  <= '0'; -- low means show
end arch;
