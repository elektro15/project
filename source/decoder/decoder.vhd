--------------------------------------------
-- Hamming Decoder:
-- An eight bit hamming decoder
--
-- Assumes 4 parity bits
-- plus additional overall parity bit
--------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity decoder is
  port (i1, i2, i3, i4, i5, i6, i7, i8 : in  std_logic;
        i9, i10, i11, i12, i13         : in  std_logic;
        o1, o2, o3, o4, o5, o6, o7, o8 : out std_logic;
        errordouble                    : out std_logic);
end decoder;

architecture arch of decoder is
  ---- INTERNAL SIGNALS ----
  -- recomputed parities
  signal p1, p2, p4, p8, p0                     : std_logic;
  -- parity errors
  signal p1e, p2e, p4e, p8e, p0e                : std_logic;
  -- input errors
  signal i1e, i2e, i3e, i4e, i5e, i6e, i7e, i8e : std_logic;
begin
  ---- RECOMPUTED PARITIES ---
  -- hamming parities
  p1 <= i1 xor i2 xor        i4 xor i5 xor        i7       ;
  p2 <= i1 xor        i3 xor i4 xor        i6 xor i7       ;
  p4 <=        i2 xor i3 xor i4 xor                      i8;
  p8 <=                             i5 xor i6 xor i7 xor i8;
  -- overall parity
  p0 <= i1 xor i2 xor i3 xor i4 xor i5 xor i6 xor i7 xor i8
           xor i9 xor i10 xor i11 xor i12;

  ---- FIND ERRORS ----
  -- check for parity errors
  p1e <= p1 xor i9 ;
  p2e <= p2 xor i10;
  p4e <= p4 xor i11;
  p8e <= p8 xor i12;
  -- overall parity
  p0e <= p0 xor i13;
  -- find error syndrome if any
  i1e <=     p1e and     p2e and not p4e and not p8e;
  i2e <=     p1e and not p2e and     p4e and not p8e;
  i3e <= not p1e and     p2e and     p4e and not p8e;
  i4e <=     p1e and     p2e and     p4e and not p8e;
  i5e <=     p1e and not p2e and not p4e and     p8e;
  i6e <= not p1e and     p2e and not p4e and     p8e;
  i7e <=     p1e and     p2e and not p4e and     p8e;
  i8e <= not p1e and not p2e and     p4e and     p8e;

  ---- OUTPUTS ----
  -- fix single-bit error
  o1 <= i1 xor i1e;
  o2 <= i2 xor i2e;
  o3 <= i3 xor i3e;
  o4 <= i4 xor i4e;
  o5 <= i5 xor i5e;
  o6 <= i6 xor i6e;
  o7 <= i7 xor i7e;
  o8 <= i8 xor i8e;

  -- double error when no overall parity error but still hamming parity error
  errordouble <= (not p0e) and (p1e or p2e or p4e or p8e);
end arch;
