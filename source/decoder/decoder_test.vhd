library ieee;
use ieee.std_logic_1164.all;

entity decoder_test is
end decoder_test;

architecture arch of decoder_test is
  -- test-component declaration
  component decoder
    port (i1, i2, i3, i4, i5, i6, i7, i8 : in  std_logic;
          i9, i10, i11, i12, i13         : in  std_logic;
          o1, o2, o3, o4, o5, o6, o7, o8 : out std_logic;
          errordouble                    : out std_logic);
  end component;
  -- internal signal declarations
  signal clk : std_logic;
  signal i1, i2, i3, i4, i5, i6, i7, i8 : std_logic;
  signal i9, i10, i11, i12, i13         : std_logic;
  signal o1, o2, o3, o4, o5, o6, o7, o8 : std_logic;
  signal errordouble                    : std_logic;
begin
  -- unit under test
  UUT: decoder PORT MAP(i1, i2, i3, i4, i5, i6, i7, i8,
                        i9, i10, i11, i12, i13,
                        o1, o2, o3, o4, o5, o6, o7, o8,
                        errordouble);

  --assert statements to verify repsonses
  process
  begin
    i1 <= '0';
    i2 <= '0';
    i3 <= '0';
    i4 <= '0';
    i5 <= '0';
    i6 <= '0';
    i7 <= '0';
    i8 <= '0';
    i9 <= '0';
    i10 <= '0';
    i11 <= '0';
    i12 <= '0';
    i13 <= '0';
    wait for 5 ns;
    assert o1  = '0' report "test1: d1 should be 0";
    assert o2  = '0' report "test1: d2 should be 0";
    assert o3  = '0' report "test1: d3 should be 0";
    assert o4  = '0' report "test1: d4 should be 0";
    assert o5  = '0' report "test1: d5 should be 0";
    assert o6  = '0' report "test1: d6 should be 0";
    assert o7  = '0' report "test1: d1 should be 0";
    assert o8  = '0' report "test1: d8 should be 0";
    assert errordouble  = '0' report "test1: errordouble should be 0";

    i1 <= '1';
    i2 <= '0';
    i3 <= '1';
    i4 <= '0';
    i5 <= '1';
    i6 <= '0';
    i7 <= '1';
    i8 <= '0';
    i9 <= '1';
    i10 <= '1';
    i11 <= '1';
    i12 <= '0';
    i13 <= '1';
    wait for 5 ns;
    assert o1  = '1' report "test2: d1 should be 0";
    assert o2  = '0' report "test2: d2 should be 0";
    assert o3  = '1' report "test2: d3 should be 0";
    assert o4  = '0' report "test2: d4 should be 0";
    assert o5  = '1' report "test2: d5 should be 0";
    assert o6  = '0' report "test2: d6 should be 0";
    assert o7  = '1' report "test2: d1 should be 0";
    assert o8  = '0' report "test2: d8 should be 0";
    assert errordouble  = '0' report "test2: errordouble should be 0";

    i1 <= '1';
    i2 <= '0';
    i3 <= '1';
    i4 <= '0';
    i5 <= '1';
    i6 <= '0';
    i7 <= '1';
    i8 <= '0';
    i9 <= '1';
    i10 <= '1';
    i11 <= '1';
    i12 <= '1';
    i13 <= '1';
    wait for 5 ns;
    assert o1  = '1' report "test3: d1 should be 0";
    assert o2  = '0' report "test3: d2 should be 0";
    assert o3  = '1' report "test3: d3 should be 0";
    assert o4  = '0' report "test3: d4 should be 0";
    assert o5  = '1' report "test3: d5 should be 0";
    assert o6  = '0' report "test3: d6 should be 0";
    assert o7  = '1' report "test3: d1 should be 0";
    assert o8  = '0' report "test3: d8 should be 0";
    assert errordouble  = '0' report "test3: errordouble should be 0";

    i1 <= '1';
    i2 <= '0';
    i3 <= '1';
    i4 <= '0';
    i5 <= '1';
    i6 <= '0';
    i7 <= '1';
    i8 <= '0';
    i9 <= '1';
    i10 <= '1';
    i11 <= '1';
    i12 <= '0';
    i13 <= '0';
    wait for 5 ns;
    assert o1  = '1' report "test4: d1 should be 0";
    assert o2  = '0' report "test4: d2 should be 0";
    assert o3  = '1' report "test4: d3 should be 0";
    assert o4  = '0' report "test4: d4 should be 0";
    assert o5  = '1' report "test4: d5 should be 0";
    assert o6  = '0' report "test4: d6 should be 0";
    assert o7  = '1' report "test4: d1 should be 0";
    assert o8  = '0' report "test4: d8 should be 0";
    assert errordouble  = '0' report "test4: errordouble should be 0";

    i1 <= '1';
    i2 <= '0';
    i3 <= '1';
    i4 <= '0';
    i5 <= '1';
    i6 <= '0';
    i7 <= '1';
    i8 <= '0';
    i9 <= '1';
    i10 <= '1';
    i11 <= '0';
    i12 <= '0';
    i13 <= '0';
    wait for 5 ns;
    assert o1  = '1' report "test5: d1 should be 0";
    assert o2  = '0' report "test5: d2 should be 0";
    assert o3  = '1' report "test5: d3 should be 0";
    assert o4  = '0' report "test5: d4 should be 0";
    assert o5  = '1' report "test5: d5 should be 0";
    assert o6  = '0' report "test5: d6 should be 0";
    assert o7  = '1' report "test5: d1 should be 0";
    assert o8  = '0' report "test5: d8 should be 0";
    assert errordouble  = '1' report "test5: errordouble should be 1";
   end process;
end arch;
