library ieee;
use ieee.std_logic_1164.all;

entity encoder is
  port (i1, i2, i3, i4, i5, i6, i7, i8 : in std_logic;
        o1, o2, o3, o4, o5, o6, o7, o8 : out std_logic;
        o9, o10, o11, o12, o13         : out std_logic);
end encoder;

architecture arch of encoder is
  signal p0, p1, p2, p4, p8 : std_logic;
begin
  -- hamming parity bits
  p1 <= i1 xor i2 xor i4 xor i5 xor i7;
  p2 <= i1 xor i3 xor i4 xor i6 xor i7;
  p4 <= i2 xor i3 xor i4 xor i8;
  p8 <= i5 xor i6 xor i7 xor i8;

  -- overall parity
  p0 <= i1 xor i2 xor i3 xor i4 xor i5 xor i6 xor i7 xor i8
           xor p1 xor p2 xor p4 xor p8;

  -- outputs
  o1  <= i1;
  o2  <= i2;
  o3  <= i3;
  o4  <= i4;
  o5  <= i5;
  o6  <= i6;
  o7  <= i7;
  o8  <= i8;
  o9  <= p1;
  o10 <= p2;
  o11 <= p4;
  o12 <= p8;
  o13 <= p0;
end arch;
