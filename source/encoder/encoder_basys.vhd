library ieee;
use ieee.std_logic_1164.all;

entity encoder_basys is
  port (
    -- input switches
    sw1, sw2, sw3, sw4, sw5, sw6, sw7, sw8 : in std_logic;
    -- output data bit connectors
    con1, con2, con3, con4, con5, con6, con7, con8 : out std_logic;
    -- output parity bit connectors
    con9, con10, con11, con12, con13 : out std_logic;
    -- leds for debugging
    led1, led2, led3, led4, led5, led6, led7, led8 : out std_logic;
    -- digital number displays for debugging
    seg1, seg2, seg3, seg4, seg5, seg6, seg7, seg8 : out std_logic;
    segdp : out std_logic;
    dgt1, dgt2, dgt3, dgt4 : out std_logic);
end encoder_basys;

-- this is the architecture
architecture arch of encoder_basys is
  component encoder
    port (i1, i2, i3, i4, i5, i6, i7, i8 : in std_logic;
          o1, o2, o3, o4, o5, o6, o7, o8 : out std_logic;
          o9, o10, o11, o12, o13         : out std_logic);
  end component;

  signal d1, d2, d3, d4, d5, d6, d7, d8 : std_logic;
  signal p1, p2, p4, p8, p0             : std_logic;
  signal nodigits                       : std_logic;
begin
  instance: encoder PORT MAP (
    i1 => sw1,
    i2 => sw2,
    i3 => sw3,
    i4 => sw4,
    i5 => sw5,
    i6 => sw6,
    i7 => sw7,
    i8 => sw8,
    o1 => d1,
    o2 => d2,
    o3 => d3,
    o4 => d4,
    o5 => d5,
    o6 => d6,
    o7 => d7,
    o8 => d8,
    o9 => p1,
    o10 => p2,
    o11 => p4,
    o12 => p8,
    o13 => p0
  );

  ---- OUTPUTS ----
  -- pipe input bits directly through output pins
  con1  <= d1;
  con2  <= d2;
  con3  <= d3;
  con4  <= d4;
  con5  <= d5;
  con6  <= d6;
  con7  <= d7;
  con8  <= d8;
  con9  <= p1;
  con10 <= p2;
  con11 <= p4;
  con12 <= p8;
  con13 <= p0;

  ---- DEBUGGING ----
  -- for debugging we also display the bit string on the leds
  led1 <= d1;
  led2 <= d2;
  led3 <= d3;
  led4 <= d4;
  led5 <= d5;
  led6 <= d6;
  led7 <= d7;
  led8 <= d8;

  -- additionally display the computed parity bits on digital numbers display
  nodigits <= not p1 and not p2 and not p4 and not p8;
  -- zero means show
  segdp <= not p0;
  dgt1 <= not (p1 or nodigits);
  dgt2 <= not (p2 or nodigits);
  dgt3 <= not (p4 or nodigits);
  dgt4 <= not (p8 or nodigits);

  -- zero means show
  seg1 <= nodigits;
  seg2 <= nodigits;
  seg3 <= nodigits;
  seg4 <= nodigits;
  seg5 <= nodigits;
  seg6 <= nodigits;
  seg7 <= nodigits;
end arch;
