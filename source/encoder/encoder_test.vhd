library ieee;
use ieee.std_logic_1164.all;

entity encoder_test is
end encoder_test;

architecture arch of encoder_test is
  -- test-component declaration
  component encoder
    port (i1, i2, i3, i4, i5, i6, i7, i8 : in std_logic;
          o1, o2, o3, o4, o5, o6, o7, o8 : out std_logic;
          o9, o10, o11, o12, o13         : out std_logic);
  end component;
  -- internal signal declarations
  signal clk : std_logic;
  signal i1, i2, i3, i4, i5, i6, i7, i8 : std_logic;
  signal o1, o2, o3, o4, o5, o6, o7, o8 : std_logic;
  signal o9, o10, o11, o12, o13         : std_logic;
begin
  -- unit under test
  UUT: encoder PORT MAP(i1, i2, i3, i4, i5, i6, i7, i8,
                        o1, o2, o3, o4, o5, o6, o7, o8,
                        o9, o10, o11, o12, o13);

  --assert statements to verify repsonses
  process
  begin
    i1 <= '0';
    i2 <= '0';
    i3 <= '0';
    i4 <= '0';
    i5 <= '0';
    i6 <= '0';
    i7 <= '0';
    i8 <= '0';
    wait for 5 ns;
    assert o1  = '0' report "test1: d1 should be 0";
    assert o2  = '0' report "test1: d2 should be 0";
    assert o3  = '0' report "test1: d3 should be 0";
    assert o4  = '0' report "test1: d4 should be 0";
    assert o5  = '0' report "test1: d5 should be 0";
    assert o6  = '0' report "test1: d6 should be 0";
    assert o7  = '0' report "test1: d1 should be 0";
    assert o8  = '0' report "test1: d8 should be 0";
    assert o9  = '0' report "test1: p1 should be 0";
    assert o10 = '0' report "test1: p2 should be 0";
    assert o11 = '0' report "test1: p4 should be 0";
    assert o12 = '0' report "test1: p8 should be 0";
    assert o13 = '0' report "test1: p0 should be 0";

    i1 <= '1';
    i2 <= '0';
    i3 <= '1';
    i4 <= '0';
    i5 <= '1';
    i6 <= '0';
    i7 <= '1';
    i8 <= '0';
    wait for 5 ns;
    assert o9  = '1' report "test2: p1 should be 1";
    assert o10 = '1' report "test2: p2 should be 0";
    assert o11 = '1' report "test2: p4 should be 1";
    assert o12 = '0' report "test2: p8 should be 0";
    assert o13 = '1' report "test2: p0 should be 0";

    i1 <= '1';
    i2 <= '1';
    i3 <= '1';
    i4 <= '1';
    i5 <= '1';
    i6 <= '1';
    i7 <= '1';
    i8 <= '1';
    wait for 5 ns;
    assert o9  = '1' report "test3: p1 should be 1";
    assert o10 = '1' report "test3: p2 should be 1";
    assert o11 = '0' report "test3: p4 should be 0";
    assert o12 = '0' report "test3: p8 should be 0";
    assert o13 = '0' report "test3: p0 should be 0";

    i1 <= '0';
    i2 <= '0';
    i3 <= '0';
    i4 <= '0';
    i5 <= '1';
    i6 <= '1';
    i7 <= '1';
    i8 <= '1';
    wait for 5 ns;
    assert o9  = '0' report "test4: p1 should be 0";
    assert o10 = '0' report "test4: p2 should be 0";
    assert o11 = '1' report "test4: p4 should be 1";
    assert o12 = '0' report "test4: p8 should be 0";
    assert o13 = '1' report "test4: p0 should be 1";
   end process;
end arch;
